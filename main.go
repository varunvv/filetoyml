package main

import (
	"bytes"
	"flag"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"gopkg.in/yaml.v2"
)

func typify(x string) interface{} {
	i, err := strconv.ParseInt(x, 10, 64)
	if err != nil {
		return x
	}
	return i
}

var (
	dir     *string
	out     *string
	summary *bool
	graphs  *bool
)

func main() {
	var (
		dest io.WriteCloser
		data map[interface{}]interface{}
		y    []byte
		err  error
	)
	dir = flag.String("d", "", "Directory contain the files to be merged to yaml")
	out = flag.String("o", "", "Output file. default is stdout")
	summary = flag.Bool("s", false, "Should generate summary queries")
	graphs = flag.Bool("g", true, "Should generate graph queries")
	flag.Parse()
	if *out == "" {
		log.Fatal("Empty output file")
	} else {
		if dest, err = os.Create(*out); err != nil {
			log.Fatal(err)
		}
	}
	if data, err = convert(*dir, false); err != nil {
		log.Fatal(err)
	}
	if y, err = yaml.Marshal(data); err != nil {
		log.Fatal(err)
	}
	if err = writeOut(dest, y); err != nil {
		log.Fatal(err)
	}
}

func convert(dir string, isSummary bool) (contents map[interface{}]interface{}, err error) {
	var (
		files []os.FileInfo
		p     string
		f     *os.File
	)
	if files, err = ioutil.ReadDir(dir); err != nil {
		return
	}
	contents = map[interface{}]interface{}{}
	buf := new(bytes.Buffer)
	for _, file := range files {
		if strings.HasPrefix(file.Name(), ".") {
			continue
		}
		if p, err = filepath.Abs(dir); err != nil {
			return
		}
		if file.IsDir() {
			if !*summary && strings.HasPrefix(file.Name(), "summary_") {
				continue
			}
			if contents[typify(file.Name())], err = convert(filepath.Join(p, file.Name()), strings.HasPrefix(file.Name(), "summary_")); err != nil {
				return
			}
			continue
		}
		if !*graphs && !isSummary {
			continue
		}
		if f, err = os.Open(filepath.Join(p, file.Name())); err != nil {
			return
		}
		if _, err = io.Copy(buf, f); err != nil {
			return
		}
		r3 := regexp.MustCompile(`(?m:^\s+)`)
		r2 := regexp.MustCompile(`(?m:\s+$)`)
		r1 := regexp.MustCompile(`[\n\t]`)
		contents[typify(file.Name())] = r1.ReplaceAllString(r2.ReplaceAllString(r3.ReplaceAllString(strings.TrimSpace(buf.String()), " "), " "), " ") + "\n"
		buf.Reset()
	}
	return
}

func writeOut(w io.WriteCloser, data []byte) (err error) {
	b := bytes.NewBuffer(data)
	_, err = io.Copy(w, b)
	if err != nil {
		return
	}
	return w.Close()
}

// filetoyml -d=/home/varun/gopath/src/git.hifx.in/lens/redshiftquerybuilder/template -o /home/varun/gopath/src/git.hifx.in/lens/genie/resources/queries.yml
